//
//  DetailsViewController.swift
//  NewsAppTask
//
//  Created by Vijju on 17/12/19.
//  Copyright © 2019 BItra vidyardhi. All rights reserved.
//

import UIKit
import AVKit

class DetailsViewController: UIViewController {
    
    var titleStr = ""
    var discStr = ""
    var imgStr = ""
    var authorStr = ""
    
    @IBOutlet weak var shwImg: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var discLbl: UILabel!
    
    @IBOutlet weak var authorLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Details"
        
        UINavigationBar.appearance().isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 25)!, NSAttributedString.Key.foregroundColor: UIColor.black]
        titleLbl.text = titleStr
        authorLbl.text = authorStr
        discLbl.text = discStr
        //cell.author.text = self.articles?[indexPath.item].author
        if imgStr == "null" || imgStr == "default image"{
            
        }else{
            shwImg.downloadImage(from: imgStr)
            
            //                let imageURL:URL=URL(string: imageUrlStr)!
            //                let data=NSData(contentsOf: imageURL)
            //                cell.showImg.image=UIImage(data: data! as Data)
        }
        
        if authorStr == "null"{
            authorLbl.text = "Not Available"
        }else{
            authorLbl.text = authorStr
            
        }
        
        
        if titleStr == "null"{
            titleLbl.text = "Not Available"
        }else{
            titleLbl.text = titleStr
            
        }
        
    }
    

    
   
    
}
