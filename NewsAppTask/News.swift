//
//  News.swift
//  NewsAppTask
//
//  Created by Vijju on 17/12/19.
//  Copyright © 2019 BItra vidyardhi. All rights reserved.
//

import UIKit

class News: NSObject {
    
    var headline:String?
    var author:String?
    var url:String?
    var imageUrl:String?
    var desc:String?
    var date:String?
    
}
