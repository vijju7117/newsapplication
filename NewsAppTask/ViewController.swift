//
//  ViewController.swift
//  NewsAppTask
//
//  Created by Vijju on 17/12/19.
//  Copyright © 2019 BItra vidyardhi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
   
    @IBOutlet weak var newsTableView: UITableView!
    
    
    
    
    var articles: [News]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "NorthEastWestSouth"
        
        UINavigationBar.appearance().isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 25)!, NSAttributedString.Key.foregroundColor: UIColor.black]
        
        self.newsTableView.delegate = self
        self.newsTableView.dataSource = self
        self.newsTableView.register(UINib(nibName: "NewsReader", bundle: nil), forCellReuseIdentifier: "NewsReader")
        self.newsTableView.backgroundColor = UIColor.clear
        fetchArticles()
    }
    
    func fetchArticles(){
        let urlRequest = URLRequest(url: URL(string: "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=0ff4394fa60f4402a4e501451f466427")!)
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data,response,error) in
            
            if error != nil {
                print(error)
                return
            }
            
            self.articles = [News]()
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                
                if let articlesFromJson = json["articles"] as? [[String : AnyObject]] {
                    for articleFromJson in articlesFromJson {
                        let article = News()
                        if let title = articleFromJson["title"] as? String, let author = articleFromJson["author"] as? String, let desc = articleFromJson["description"] as? String, let url = articleFromJson["url"] as? String, let date = articleFromJson["publishedAt"] as? String, let urlToImage = articleFromJson["urlToImage"] as? String {
                            
                            article.author = author
                            article.desc = desc
                            article.headline = title
                            article.url = url
                            article.date = date
                            article.imageUrl = urlToImage
                        }
                        self.articles?.append(article)
                    }
                }
                DispatchQueue.main.async {
                    self.newsTableView.reloadData()
                }
                
            } catch let error {
                print(error)
            }
            
            
        }
        
        task.resume()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = newsTableView.dequeueReusableCell(withIdentifier: "NewsReader", for: indexPath) as! NewsReader
        if self.articles!.count > 0 {
            cell.title.text = self.articles?[indexPath.item].headline
            cell.desc.text = self.articles?[indexPath.item].desc
            //cell.author.text = self.articles?[indexPath.item].author
            let imgurl = (self.articles?[indexPath.item].imageUrl) ?? "default image"
            print(imgurl)
            if imgurl == "null" || imgurl == "default image"{
            }else{
                let imageUrlStr = (self.articles?[indexPath.item].imageUrl)!
                cell.imgView.downloadImage(from: imageUrlStr)

            }
            let author =  self.articles?[indexPath.item].author
            if author == "null"{
                cell.author.text = "Not Available"
            }else{
                cell.author.text = author
            }
            let date =  self.articles?[indexPath.item].date
            if date == "null"{
                cell.date.text = "Not Available"
            }else{
                cell.date.text = date
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let reqVC = mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController

        if articles!.count > 0 {
            
            let title = (self.articles?[indexPath.item].headline) ?? "title"
            if title == "null"{
                 reqVC.titleStr = "Not Available"
            }else
            {
                reqVC.titleStr = title
            }
            
            let descp = (self.articles?[indexPath.item].desc) ?? "Description"
            if descp == "null"{
                reqVC.discStr = "Not Available"
            }else
            {
                reqVC.discStr = descp
            }
            
            let arthr = (self.articles?[indexPath.item].author) ?? "Author"
            if arthr == "null"{
                reqVC.authorStr = "Not Available"
            }else
            {
                reqVC.authorStr = arthr
            }
            
            let imgurl = (self.articles?[indexPath.item].imageUrl) ?? "default image"
            reqVC.imgStr = imgurl
        }
        self.navigationController?.pushViewController(reqVC, animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles!.count
    }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 160
        }

    
}

extension UIImageView {
    
    func downloadImage(from url: String){
        
        let urlRequest = URLRequest(url: URL(string: url)!)
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data,response,error) in
            
            if error != nil {
                print(error)
                return
            }
            
            DispatchQueue.main.async {
                self.image = UIImage(data: data!)
            }
        }
        task.resume()
    }
}































